<?php

namespace User\Model;

/**
 * Class User
 *
 * @package User\Model
 */
class User
{
    /** @var string */
    private $username;

    /** @var string */
    private $email;

    /** @var string */
    private $password;

    /** @var int */
    private $age;

    /**
     * User constructor.
     *
     * @param string $username
     * @param string $email
     * @param string $password
     * @param int $age
     */
    public function __construct($username, $email, $password, $age)
    {
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }
}
