<?php

namespace User\Service\Registration;

use User\Exception\AuthenticationFailedException;
use User\Exception\UserCreationFailedException;
use User\Exception\UserRegistrationFailedException;
use User\Model\User;

/**
 * Class UserRegistration
 *
 * @package User\Service\Registration
 */
class UserRegistration implements UserRegistrationInterface
{
    /** @var CreateUserInterface */
    private $createUserProcess;

    /** @var AuthenticationInterface */
    private $authenticationProcess;

    /**
     * UserRegistration constructor.
     * @param CreateUserInterface $createUserProcess
     * @param AuthenticationInterface $authenticationProcess
     */
    public function __construct(CreateUserInterface $createUserProcess, AuthenticationInterface $authenticationProcess)
    {
        $this->createUserProcess = $createUserProcess;
        $this->authenticationProcess = $authenticationProcess;
    }

    /**
     * @param User $user
     * @return void
     * @throws UserRegistrationFailedException
     */
    public function register(User $user)
    {
        try {
            $this->createUserProcess->create($user);
            $this->authenticationProcess->authenticate($user);
        } catch (UserCreationFailedException $exception) {
            throw new UserRegistrationFailedException("User creation failed.");
        } catch (AuthenticationFailedException $exception) {
            throw new UserRegistrationFailedException("Authentication failed.");
        }
    }
}
