<?php

namespace User\Service\Registration;

use User\Exception\AuthenticationFailedException;
use User\Model\User;

interface AuthenticationInterface
{
    /**
     * @param User $user
     * @return void
     * @throws AuthenticationFailedException
     */
    public function authenticate(User $user);
}
