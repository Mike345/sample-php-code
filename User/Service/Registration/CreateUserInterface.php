<?php

namespace User\Service\Registration;

use User\Exception\UserRegistrationFailedException;
use User\Model\User;

/**
 * Interface CreateUserInterface
 *
 * @package User\Service\Registration
 */
interface CreateUserInterface
{
    /**
     * @param User $user
     * @return void
     * @throws UserRegistrationFailedException
     */
    public function create(User $user);
}
