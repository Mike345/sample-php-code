<?php

namespace User\Service\Registration;

use User\Exception\UserRegistrationFailedException;
use User\Model\User;

/**
 * Interface UserRegistrationInterface
 *
 * @package User\Service\Registration
 */
interface UserRegistrationInterface
{
    /**
     * @param User $user
     * @return void
     * @throws UserRegistrationFailedException
     */
    public function register(User $user);
}
