<?php

namespace User\Exception;

use Exception;

/**
 * Class UserRegistrationFailedException
 *
 * @package User\Exception
 */
class UserRegistrationFailedException extends Exception
{
    /** @var string */
    protected $message;

    /**
     * UserRegistrationFailedException constructor.
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message, $code = 0, Exception $previous = null)
    {
        $this->message = "Registration failed with message: " . $message;

        parent::__construct($message, $code, $previous);
    }
}
