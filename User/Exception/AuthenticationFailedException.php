<?php

namespace User\Exception;

use Exception;

/**
 * Class AuthenticationFailedException
 *
 * @package User\Exception
 */
class AuthenticationFailedException extends Exception
{

}
