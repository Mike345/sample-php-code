<?php

namespace User\Exception;

use Exception;

/**
 * Class UserCreationFailedException
 *
 * @package User\Exception
 */
class UserCreationFailedException extends Exception
{

}